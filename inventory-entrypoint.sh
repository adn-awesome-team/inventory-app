#!/bin/sh
set -e
# source .env

# Create necessary directories and set permissions
mkdir -p /var/run/postgresql
chown -R postgres:postgres /var/run/postgresql
chown -R postgres:postgres "$PGDATA"  # Add this line

# Initialize the database cluster if it's not already initialized
if [ ! -s "$PGDATA/PG_VERSION" ]; then
    su-exec postgres initdb
fi

# Start PostgreSQL
su-exec postgres postgres -D "$PGDATA" &
pid="$!"

# Trap the stop signal to ensure graceful shutdown
trap "echo 'Stopping PostgreSQL...'; su-exec postgres pg_ctl stop -D \"$PGDATA\" -m fast; wait $pid;" SIGINT SIGTERM

# Wait for the PostgreSQL server to start
until su-exec postgres pg_isready -h localhost -p 5432 > /dev/null 2>&1; do
    echo 'Waiting for PostgreSQL to start...'
    sleep 1
done

conf_file="/var/lib/postgresql/data/postgresql.conf"

# Modify listen_addresses in postgresql.conf
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '0.0.0.0'/" "$conf_file"

# Add custom host entry in pg_hba.conf
echo "host    movies          postgres        all         trust" >> "$PGDATA/pg_hba.conf"

# Execute scripts from /docker-entrypoint-initdb.d/
if [ -d "/docker-entrypoint-initdb.d/" ]; then
    for f in /docker-entrypoint-initdb.d/*.sh; do
        case "$f" in
            *.sh)   echo "$0: running $f"; . "$f" ;;
            *)      echo "$0: ignoring $f" ;;
        esac
        echo
    done
fi

# Stop PostgreSQL
#su-exec postgres pg_ctl stop -D "$PGDATA"
su-exec postgres pg_ctl stop 
# Start PostgreSQL
su-exec postgres postgres 
pid="$!"

# Wait for the process to be terminated
wait $pid