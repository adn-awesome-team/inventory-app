const express = require('express');
const { 
  getMovies, getMovieById, 
  createMovie, updateMovie, 
  deleteMovie, deleteAllMovies 
} = require('./app/controllers/movies');
const routes = require('./app/routes/routes');
const sequelize = require('./app/config/database');

const app = express();
const PORT = process.env.INVENTORYPORT || 8081;


// ========== Only gateway =========
// Middleware to block requests from outside
 const blockExternalRequests = (req, res, next) => {
    // Check if the request is from the API gateway
    console.log('Request Headers:', req.headers);
  
    const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const ipParts = ipAddress.split(':');
    const clientIp = ipParts[ipParts.length - 1];
    console.log('Request IP Address:', clientIp);
  
    // if (clientIp !== '192.168.0.7') {
    //   // Return a 403 Forbidden response
    //   return res.status(403).send('Access forbidden');
    // }
  
    // Continue to the next middleware
    next();
};
  
app.use(blockExternalRequests);
  

// ============ end only gateway =========


// Middleware to parse JSON
app.use(express.json());
app.use(routes);


async function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Connect to the database
// sequelize
//   .sync()
//   .then(() => console.log('Connected to the database'))
//   .catch((error) => console.error('Unable to connect to the database:', error));
delay(10000)
  .then(() => sequelize.sync())
  .then(() => console.log('Connected to the database'))
  .catch((error) => console.error('Unable to connect to the database:', error));

// Routes
app.get('/', (req, res) => {
  // Handle the request for the root URL
  res.send('Hello, World!');
});

app.get('/api/movies', getMovies);

app.get('/api/movies/:id', getMovieById);


app.post('/api/movies', createMovie);


app.put('/api/movies/:id', updateMovie);


app.delete('/api/movies/:id', deleteMovie);

app.delete('/api/movies', deleteAllMovies);

// Start the server
app.listen(PORT, () => {//blockExternalRequests
  // blockExternalRequests();
});
