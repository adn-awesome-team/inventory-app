#!/bin/bash
#source .env
set -e

# Wait for PostgreSQL to start
sleep 5

# Check if the 'movies' database exists
DB_EXISTS=$(psql -U "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_database WHERE datname='$POSTGRES_DB'")

# If the 'movies' database does not exist, create it
if [ "$DB_EXISTS" = '1' ]; then
    echo "Database $POSTGRES_DB already exists. Skipping creation."
else
    psql -U "$POSTGRES_USER" -d postgres -c "CREATE DATABASE $POSTGRES_DB;"
fi

# Connect to the 'movies' database and create the 'movies' table
psql -U "$POSTGRES_USER" -d $POSTGRES_DB -c "CREATE TABLE IF NOT EXISTS $POSTGRES_DB (id SERIAL PRIMARY KEY,title VARCHAR(255) NOT NULL, description TEXT);"

# Insert sample data into the 'movies' table
psql -U "$POSTGRES_USER" -d $POSTGRES_DB -c "INSERT INTO $POSTGRES_DB (title,description) VALUES ('inventory','test');"

psql -U $POSTGRES_USER -c "ALTER USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD';"