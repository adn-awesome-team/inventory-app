FROM debian:stable

RUN mkdir -p /app

WORKDIR /app

COPY package.json .

RUN apt-get update && apt-get install -y curl

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -

RUN apt-get install -y nodejs npm

COPY . ./

CMD ["npm", "start"]