const { DataTypes } = require('sequelize');
const sequelize = require('../config/database.js');
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') });

const Movie = sequelize.define(process.env.POSTGRES_DB, {
  title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, 
  {
    timestamps: false, // Disable createdAt and updatedAt fields
  });

module.exports = Movie;