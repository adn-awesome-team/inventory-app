const mockMovies = [
    { id: 1, title: 'Movie 1', description: 'Description 1' },
    { id: 2, title: 'Movie 2', description: 'Description 2' },
    { id: 3, title: 'Movie 3', description: 'Description 3'}
];
  
const findAll = async (options) => {
    if (options && options.where && options.where.title) {
      return mockMovies.filter((movie) => movie.title === options.where.title);
    }
    return mockMovies;
};
  
const findByPk = async (id) => {
    return mockMovies.find((movie) => movie.id === id);
};
  
const create = async (movieData) => {
    const newMovie = { id: mockMovies.length + 1, ...movieData };
    mockMovies.push(newMovie);
    return newMovie;
};
  
module.exports = {
    findAll,
    findByPk,
    create,
};
  