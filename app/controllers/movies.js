const Movie = require('../models/movie');

exports.getMovies = async (req, res) => {
  const { title } = req.query;

  try {
    const movies = title
      ? await Movie.findAll({ where: { title } })
      : await Movie.findAll();
    res.json(movies);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

exports.getMovieById = async (req, res) => {
  try {
    const movie = await Movie.findByPk(req.params.id);
    if (!movie) {
      return res.status(404).json({ error: 'Movie not found' });
    }
    return res.json(movie);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Server error' });
  }
};

exports.createMovie = async (req, res) => {
  const { title, description } = req.body;
  try {
    const movie = await Movie.create({ title, description });
    res.status(201).json(movie);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

exports.updateMovie = async (req, res) => {
  const { id } = req.params;
  const { title, description } = req.body;
  try {
    const movie = await Movie.findByPk(id);
    if (movie) {
      movie.title = title;
      movie.description = description;
      await movie.save();
      res.json(movie);
    } else {
      res.status(404).json({ error: 'Movie not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

exports.deleteMovie = async (req, res) => {
  const { id } = req.params;
  try {
    const movie = await Movie.findByPk(id);
    if (movie) {
      await movie.destroy();
      res.json({ message: 'Movie deleted successfully' });
    } else {
      res.status(404).json({ error: 'Movie not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

exports.deleteAllMovies = async (req, res) => {
  try {
    await Movie.destroy({ truncate: true });
    res.json({ message: 'All movies deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};
