const { getMovies, getMovieById, createMovie } = require('../movies');

jest.mock('../../models/movie');

describe('Movies Controller', () => {
  it('should get all movies', async () => {
    const req = { query: {} };
    const res = {
      json: jest.fn(),
    };
    await getMovies(req, res);
    expect(res.json).toHaveBeenCalledWith([
      { id: 1, title: 'Movie 1', description: 'Description 1' },
      { id: 2, title: 'Movie 2', description: 'Description 2' },
      { id: 3, title: 'Movie 3', description: 'Description 3'}
    ]);
  });

  it('should get a movie by id', async () => {
    const movieId = 1;
    const req = { params: { id: movieId } };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
    await getMovieById(req, res);
    expect(res.json).toHaveBeenCalledWith({ id: 1, title: 'Movie 1', description: 'Description 1' });
  });

  it('should create a new movie', async () => {
    const req = { body: { title: 'New Movie', description: 'New Description' } };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
    await createMovie(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({
      id: 4,
      title: 'New Movie',
      description: 'New Description',
    });
  });
});